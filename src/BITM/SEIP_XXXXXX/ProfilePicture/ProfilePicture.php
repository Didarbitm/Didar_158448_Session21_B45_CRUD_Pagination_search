<?php
namespace App\ProfilePicture;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $picture;
    private $tempLoacation;
    private $success;
    private $error;




    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("Name",$allPostData)){
            $this->name= $allPostData['Name'];
        }




    }

    public function setProPic($allFileDate=null){

        if(array_key_exists("Picture",$allFileDate)){


            $this->picture= $allFileDate['Picture']['name'];
            $this->tempLoacation= $allFileDate['Picture']['tmp_name'];
            $src= "img/".time().$this->picture;
            $this->success = move_uploaded_file($this->tempLoacation, $src);
            $this->error = $allFileDate['Picture']['error'];
        }


    }


    public function store(){
       $arrData  =  array($this->name,time().$this->picture);

       $query= 'INSERT INTO profile_picture (name, picture) VALUES (?,?)';

       $STH = $this->DBH->prepare($query);
       $result = $STH->execute($arrData);

       if($result){
           Message::setMessage("Success! Data has been stored successfully!");
       }
       else{
           Message::setMessage("Failed! Data has not been stored!");
       }

        Utility::redirect('index.php');

    }

    public function view(){

        $sql = "Select * from profile_picture where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function index(){

        $sql = "Select * from profile_picture where soft_deleted='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){

        $sql = "Select * from profile_picture where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }



    public function update(){
        $arrData  =  array($this->name,time().$this->picture);

        $query= 'UPDATE profile_picture SET name = ?, picture= ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }

        Utility::redirect('index.php');

    }



    public function trash(){
        $arrData  =  array("Yes");

        $query= 'UPDATE profile_picture SET soft_deleted = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Trashed!");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!");
        }

        Utility::redirect('trashed.php');

    }




    public function recover(){
        $arrData  =  array("No");

        $query= 'UPDATE profile_picture SET soft_deleted = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been Recover successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recoverd!");
        }

        Utility::redirect('index.php');

    }


    public function delete(){
        $sql = "DELETE from profile_picture WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if($result){
            Message::setMessage("Success! Data has been Recover successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recoverd!");
        }
        Utility::redirect('index.php');
    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }




    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

}











