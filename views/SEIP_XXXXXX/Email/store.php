<?php
require_once("../../../vendor/autoload.php");


use \App\EmailAddress\EmailAddress;


$objEmailAddress = new EmailAddress();

$objEmailAddress->setData($_POST);

$objEmailAddress->store();
