<?php
require_once("../../../vendor/autoload.php");


use \App\ProfilePicture\ProfilePicture;


$objProfilePic = new ProfilePicture();

$objProfilePic->setData($_POST);
$objProfilePic->setProPic($_FILES);

$objProfilePic->store();
