

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";


$objProfilePicture = new \App\ProfilePicture\ProfilePicture();
$objProfilePicture->setData($_GET);
$oneData = $objProfilePicture->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ProfilePicture Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">
   <div style="width:50%">
       <h1> Edit ProfilePicture list:</h1>
    <form class="form-group" action="update.php" method="post">

    Enter Name:
        <input class="form-control" type="text" name="bookName" value="<?php echo $oneData->name?>">
        <br>
        ProfilePicture:
        <input type="radio" name="ProfilePicture" value="mail"> Mail
        <input type="radio" name="ProfilePicture" value="femail">Femail
         <br>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>">
        <input class="form-control btn btn-primary" type="submit" value="Update">

</form>
   </div>
</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


