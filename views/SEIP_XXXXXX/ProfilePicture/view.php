

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ProfilePicture - Singel List</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            margin-top: 100px;
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>

<?php
require_once("../../../vendor/autoload.php");

$objProfilePic = new \App\ProfilePicture\ProfilePicture();
$objProfilePic->setData($_GET);
$oneData = $objProfilePic->view();


echo "
  <div class='container'>
    <h1> Single Book Information  </h1>
    <table class='table table - striped table - bordered' cellspacing='0px'>

       <tr>
           <td>ID: </td>
           <td> $oneData->id </td>
       </tr>


       <tr>
           <td>Name: </td>
           <td> $oneData->name </td>
       </tr>

          <tr>
           <td>Profile Pic: </td>
           <td><img src=' img/$oneData->picture'> </td>
       </tr>
    </table>

  </div>

";

?>

</body>
</html>
