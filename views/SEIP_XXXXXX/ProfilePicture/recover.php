<?php
require_once("../../../vendor/autoload.php");


use \App\ProfilePicture\ProfilePicture;


$objProfilePicture = new ProfilePicture();

$objProfilePicture->setData($_GET);
$objProfilePicture->recover();
