

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

  if(!isset($_SESSION)){
      session_start();
  }
  $msg = Message::getMessage();

  echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender info Create Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">
    <h2>Input Name and Gender:</h2>

<form class="form-group" action="store.php" method="post">

    Enter Name:
    <input  type="text" name="Name">
    <br>
    Gender:
    <input type="radio" name="Gender" value="mail"> Mail
    <input type="radio" name="Gender" value="femail">Femail
    <br>
    <input type="submit" class="btn btn-primary">

</form>
</div>



<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


